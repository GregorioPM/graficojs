var anios = [];

var meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun",
    "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

var Tiempo = ["Mensual", "Bimestre", "Trimestre", "Cuatrimestre", "Semestre"];

function FormIngresos() {

    var borrar = document.getElementById("ingreso");
    borrar.innerHTML = "";

    var tabla = document.getElementById("ingreso");
    var formIngreso = "<form>";
    formIngreso += "<h6 >Ingresos del Año</h6>";
    formIngreso += "<div><input class='ingresoinput' type='number' id='ani0' value='2020'></div>" + "<br>";

    for (i = 0; i < 12; i++) {
        formIngreso += "<label class='mes'>" + meses[i] + "</label>";
        formIngreso += "<input class='ingresoinput' type='number'  name='valor' value='" + ((i + 1) * 1000) + "'/><br>";
    }
    formIngreso += "<button type='button' class='btn btn-success mt-2' onclick='guardar()'>Guardar</button>";
    formIngreso += "</form>";
    tabla.innerHTML = formIngreso;
    var grafico= document.getElementById("graf");
    grafico.style.display="none";
}

function guardar() {

    var borrar = document.getElementById("ingreso");
    let nombre_anio = Number(document.getElementById("ani0").value);

    let datos = document.getElementsByName("valor");
    let vector = [];

    datos.forEach(input => {
        vector.push(input.value);
    });

    var anio = {
        nombre: nombre_anio,
        saldos: vector
    }

    anios.push(anio);

    borrar.innerHTML="";
    var grafico= document.getElementById("graf");
    grafico.style.display="block";
    
}

function mostrar() {

    var borrar = document.getElementById("ingreso");
    borrar.innerHTML = "";
    var tabla = document.getElementById("ingreso");

    var formTipo = "<form>";
    formTipo += "<select id='ing1'>";

    for (let index = 0; index < anios.length; index++) {
        formTipo += "<option>" + anios[index]["nombre"] + "</option>";
    }

    formTipo += "</select> <br> <br>";

    formTipo += "<select id='tiempos'>";
    for (let i = 0; i < Tiempo.length; i++) {
        formTipo += "<option>" + Tiempo[i] + "</option>";
    }

    formTipo += "</select> <br> <br>";
    formTipo += "<button type='button' class='btn btn-success' onclick='crearGrafico()'>Mostrar</button>";
    formTipo += "</form>";

    tabla.innerHTML = formTipo;
}

function crearGrafico() {
    var tabla = document.getElementById("Mostra");
    let a = document.getElementById("ing1").value;
    let b = document.getElementById("tiempos").value;
    let valor;
    let tipo;

    for (let index = 0; index < anios.length; index++) {
        if (anios[index]["nombre"] == a) {
            valor = anios[index]["saldos"];
        };
    }

    for (let index = 0; index < Tiempo.length; index++) {
        if (Tiempo[index] == b) {
            tipo = index + 1;
        }
    }

    if (tipo == 5) {
        tipo++;
    }

    input = operar(valor, tipo);
    
    draw_chart(input["grupos"],input["saldos"]);

}

function operar(valores, pareja) {
    let salida = [];
    let costo = [];
    let cantidad = 0;

    cantidad = pareja == 1 ? 12 : pareja == 2 ? 6 : pareja == 3 ? 4 : pareja == 4 ? 3 : 2;

    let j = 0;
    let p = pareja;
    let suma = 0;

    for (let i = 0; i < cantidad; i++) {
        for (let k = j; k <= j + p - 1; k++) {
            suma += Number(valores[k]);
        }
        salida.push(meses[j] + "-" + meses[j + p - 1]);
        costo.push(suma);
        j += p;
        suma = 0;
    }

    var out = {
        grupos: salida,
        saldos: costo
    }
    return out;
}

function draw() {

    drawChart(57);
    google.charts.setOnLoadCallback(drawChart);

}

function draw_chart(datos, valores) {
    drawChart(datos, valores);
    drawTable(datos, valores);
    google.charts.setOnLoadCallback(drawChart);
    google.charts.setOnLoadCallback(drawTable);

}